from knn import *
import matplotlib.pyplot as plt
import numpy as np


# 将标签转换为数值
def conver(y_pre,y_true):
    y_pre1 = []
    y_true1 = []
    for i in range(len(y_true)):
        if y_true[i] == "Iris-virginica":
            y_true1.append(0)
        if y_true[i] == "Iris-versicolor":
            y_true1.append(1)  
        if y_true[i] == "Iris-setosa":
            y_true1.append(2)

        if y_pre[i] == "Iris-virginica":
            y_pre1.append(0)
        if y_pre[i] == "Iris-versicolor":
            y_pre1.append(1) 
        if y_pre[i] == "Iris-setosa":
            y_pre1.append(2)
    return y_pre1,y_true1


# 计算TP  一个正例被正确预测为正例
# , FP   一个反例被正确预测为反例
# , TN   一个反例被错误预测为正例
# , FN   一个正例被错误预测为反例
# 计算TP,FP,TN,FN
def perf_measure(y_true,y_pred):
    TP, FP, TN, FN = 0, 0, 0, 0
    matx = np.zeros((3,3))
    for i in range(len(y_true)):
        matx[y_true[i]][y_pred[i]] += 1
    TP = np.diag(matx)
    FP = matx.sum(axis=0) - TP
    FN = matx.sum(axis=1) - TP
    TN = matx.sum() - (TP + FP + FN)
    FPR = FP/(FP+TN)
    TPR = TP/(TP+FN)
    print(FP,TP,FN,FPR,TPR)
    return TP/(FP+TP),TP/(FN+TP),FPR,TPR
    


def drawpr(x,y,x_table,y_table,title):
    plt.rc("font",family="FangSong")
    plt.plot(x,y,marker='o')
    plt.xlabel(x_table,fontsize=20)
    plt.ylabel(y_table,fontsize=20)
    plt.title(title,fontsize=20)
    plt.show() 

pr_array = []
re_array = []
fpr_array = []
tpr_array = []

for i in range(1,13,1):
    for j in np.arange(0.1, 0.6, 0.1):
        y_pre,y_true = testClass(j,i)
        y_pre,y_true = conver(y_pre,y_true)
        pr,re,fpr,tpr= perf_measure(y_true=y_true,y_pred=y_pre)
        pr_array.append(pr)
        re_array.append(re)
        fpr_array.append(fpr)
        tpr_array.append(tpr)
      
drawpr(re_array,pr_array,"Recall","Precision","PR曲线")
drawpr(fpr_array,tpr_array,"FPR","TPR","ROC曲线")     