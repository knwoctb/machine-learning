from collections import Counter, defaultdict
from random import shuffle
from numpy import *  #导入科学计算包Numpy模块
import numpy as np
import operator    #导入运算符模块
import os
import pandas as pd
 

# 模拟已知数据集 后续编程中直接使用现成的数据集，无需造数据
def createDateSet():
    group = array([[1.0,1.1],
    [1.0,1.0],
    [0,0],
    [0,0.1]])
    labels = ['A','A','B','B']
    return group,labels

# 数据归一化处理
def norm(Mat):
    # 获取每一列的最大最小值，返回一个矩阵
    min_type = Mat.min(0)
    max_type = Mat.max(0)
    different = max_type - min_type
    newMat = zeros(Mat.shape)
    newMat = Mat - tile(min_type,(Mat.shape[0],1))
    newMat = newMat/tile(different,(Mat.shape[0],1))
    print(len(Mat))
    print(len(newMat))
    # print(newMat)
    # print("===================================")
    # print(Mat)
    return newMat
   


# 数据准备   
def getData1(path):
    fr = open(path)

    # 读取指定路径的文件内容
    fileData = fr.readlines()
    fileLength = len(fileData)
    
    # 在读取数据时没有排除标题行，故实际数据共有 fileLength - 1行
    mat = zeros((fileLength-1,4))  # 生成fileLength-1 * 4的矩阵
    index = 0
    label = []   # 存储标签
    for line in fileData:
        if index > 0:   # 用于排除标题行
           
            line = line.strip()
            line = line.split(",")
            label.append(line[-1])
            mat[index-1:] = line[0:len(line)-1]  
        index += 1
    print(label)

    
    return norm(mat),label



# 数据准备
def getData(path):
    
    names = ['sepal_length','sepal_width','petal_length','petal_width','species']
    # 使用pandas中的函数读取.csv文件
    trains = pd.read_csv(path,header=0,names=names)
    
    mat = zeros((len(trains),4))   # 生成len(trains) * 4的矩阵
    index = 0
    label = [] # 存储标签

   

    for i in range(len(trains)):
        label.append(trains[names[-1]][i])
        mat[index:] = trains[names[0:4]]

    return norm(mat),label
    


# 距离计算
def distance(goalSet,dataSet):
     # 获取测试集的一维数组长度
    dataSetSize = dataSet.shape[0]
     # 复制copygoal，返回datasetSize行1列的数组 
    copygoal = tile(goalSet,(dataSetSize,1))
     # 开始计算当前点与已知数据集的点的距离
    diffMat = copygoal - dataSet
    powdiffMat = diffMat**2
    diffMat = powdiffMat.sum(axis=1)  #按行求和
    Mat = diffMat**0.5
    # 获取数组从小到达的索引（数组下标）
    sortedDistance = Mat.argsort()  
    # print(Mat)
    return sortedDistance



# k近邻算法核心实现
def classfy0(inX, dataSet, labels, k):
    # 距离计算（欧式距离）
    sortedDistance = distance(inX,dataSet)
    classcount = {}
    # 统计距离当前点最近的k个标签的个数
    for i in range(k):
        Visable = labels[sortedDistance[i]]
        classcount[Visable] = classcount.get(Visable,0) + 1
    # 对统计出的标签数从小到大排序
    sortedClassnumber = sorted(classcount.items(),
    key=operator.itemgetter(1),reverse=True)
    # 返回预测值
    return sortedClassnumber[0][0]


# 算法测试 
def testClass(textPro,k):
    
    # 统计错误个数
    erroConut = 0.0
    group,label = getData(os.path.dirname(os.path.abspath(__file__)) + "\\IRIS1.csv")

    # group,label = createDateSet()
    textSize = int(textPro*group.shape[0])
    pre = []
    for i in range(textSize):
        textresult = classfy0(group[i,:],group[textSize:group.shape[0]],label[textSize:group.shape[0]],k)
        pre.append(textresult) 
        print("预测结果:%s,真实结果:%s" % (textresult,label[i]))
        if (textresult != label[i]):
            erroConut += 1.0
    print("错误次数：%d " % erroConut)
    print("错误百分比：%f %%" % (erroConut/textSize*100))

    return pre,label[0:textSize]

# testClass(0.4,90)


# getData1(os.path.dirname(os.path.abspath(__file__)) + "\\IRIS.csv")










     
    
    






