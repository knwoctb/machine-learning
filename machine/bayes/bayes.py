import numpy as np
import os
import re
import math


dirpath = os.path.dirname(os.path.abspath(__file__))
dalist_path = dirpath+"\Ch04"



# 导入文本数据
def getData(textclass):
    datapath = dalist_path+"\email\\"+textclass  # 文本存储路径
    filename = os.listdir(datapath)
    wordlist = []   # 存储拆分出来的单词
    count = 0
    for name in filename:
       
       if count < 10:
            fr = open(datapath+"\\"+name)
            data = fr.readlines()  # 读取文件内容
            for i in data:
                i = i.strip()
                word = re.findall('[a-zA-Z]+',i)  # 正则表达式提取单词
                if len(word) != 0:
                    for j in word:
                        wordlist.append(j)  # 将提取出来的单词添加到列表中
            count = count + 1
    return amount(wordlist=wordlist,textclass=textclass)

# 统计同类型文本数量以及文本中单词出现的次数,以及总文本数量
def amount(wordlist,textclass):
    datapath = dalist_path+"\email\\"+textclass
    classnum =  len(os.listdir(datapath))  # 同类型文本数量
    wordsum = len(wordlist)  # 总的单词数
    
    wordnum = {}   # 每个单词出现的次数
    for i in wordlist:
       wordnum[i] = wordnum.get(i,0) + 1
    return wordsum,wordnum,classnum


def computs(testemail,wdnum,wdsum):
    proba = 0
    for i in testemail:
        i = i.strip()  # 去除换行符
        word = re.findall('[a-zA-Z]+',i)  # 正则表达式提取单词
        if len(word) != 0:
            for j in word:
                if j in wdnum:
                    proba = proba + math.log(wdnum[j]/wdsum)  # 对运算结果取对数，防止出现结果下溢
                
    return proba

def bayes(testemail):
    spam_wdsum,spam_wdnum,spam_num = getData("spam")  # 计算训练集中的单词总数，每个单词出现的次数以及共有多少同类型文本
    ham_wdsum,ham_wdnum,ham_num = getData("ham")     # 计算训练集中的单词总数，每个单词出现的次数以及共有多少同类型文本

    prob_ham = math.log(ham_num/(ham_num+spam_num))  # 计算正常邮件出现的概率
    prob_spam = math.log(spam_num/(ham_num+spam_num))   # 计算垃圾邮件出现的概率

    # print(prob_ham)
    # prob_ham = ham_num/(ham_num+spam_num)
    # prob_spam = spam_num/(ham_num+spam_num)

    # print(computs(testemail=testemail,wdnum=ham_wdnum,wdsum=ham_wdsum))
    # print(computs(testemail=testemail,wdnum=spam_wdnum,wdsum=spam_wdsum))

    prob_ham = prob_ham + computs(testemail=testemail,wdnum=ham_wdnum,wdsum=ham_wdsum)  # 计算属于该类别的概率
    prob_spam = prob_spam + computs(testemail=testemail,wdnum=spam_wdnum,wdsum=spam_wdsum)
    
    return "ham" if prob_ham<prob_spam else "spam"


text= ['10.txt','11.txt','12.txt','13.txt','14.txt','15.txt','16.txt','17.txt','23.txt','25.txt']
for i in text:
    datapath = dalist_path+"\email\\"+"spam\\"+i
    data = open(datapath).readlines()
    print("预测结果%s,真实结果%s" % (bayes(data),"spam"))

# 问题 在对条件概率取对数后分类结果总是和真实结果相反-----》解决: 由于在位取对数之前条件概率小于1，因此会越加越小，越小的越接近真实结果


 
