import jieba
import numpy as np
import os
import re

dirpath = os.path.dirname(os.path.abspath(__file__))
dalist_path = dirpath+"\Ch04\\SogouC\\Sample"

def getData():
    datapath = dalist_path+""
    dirname = os.listdir(datapath)
    stopword = [line.strip() for line in open(dirpath+"\\Ch04\\stopwords_cn.txt",encoding='utf-8').readlines()]  # 加载停用词



    for dir in dirname:
        filename = os.listdir(datapath+"\\"+dir)
        for name in filename:
            fr = open(datapath+"\\"+dir+"\\"+name,encoding='utf-8')
            data = fr.readlines()
            for i in data:
                 i = re.sub('\W*', '', i)
                 print(jieba.lcut(i))
    return 

getData()